# Splatoon Utilities
## Download SplatoonUtilities by clicking [here](https://github.com/MCMiners9/SplatoonUtilities/releases)

## What is SplatoonUtilities?
### SplatoonUtilities is a group of tools like Language Switching, OctoSFX, GeckoMapTester put into 1 repository.

# HUGE credits to [OatmealDome](https://github.com/OatmealDome) for making the original SplatoonUtilities repository and for creating the tools inside.
